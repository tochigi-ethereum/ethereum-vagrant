# Vagrant environment for Ethereum

## Installation

* Install VirtualBox [download](https://www.virtualbox.org/wiki/Downloads) (I'm using version version 5.2.8)

* Install Vagrant [installation instructions](http://docs.vagrantup.com/v2/installation/index.html) (I'm using version 2.0.3) 

* Clone this repository and cd into it

    ```$ git clone https://gitlab.com/tochigi-ethereum/ethereum-vagrant.git```

* run vagrant (for the first time it should take up to 10-15 min)

    ```$ vagrant up```

## Installed components

* [Node.js](http://nodejs.org/)
* [npm](https://npmjs.org/)
* [Geth(Go Ethereum)](https://geth.ethereum.org)
* [Solidity](https://github.com/ethereum/solidity)
* [Truffle](http://truffleframework.com)
* [Ganache CLI](http://truffleframework.com/ganache)
* [Remix IDE (aka. Browser-Solidity)](https://github.com/ethereum/remix-ide)


## IP Address

```
config.vm.network "private_network", ip: "192.168.33.100"
```

## Port Forwarding

```
# for Remix-ide
config.vm.network "forwarded_port", guest: 8080, host: 8080, auto_correct: true
# for JSON-RPC
config.vm.network "forwarded_port", guest: 8545, host: 8545, auto_correct: true
```

## Usage

### Remix

* run Remix

    ```
    $ vagrant ssh
    $ remix-ide
    ```
    
* http://localhost:8080

## Thanks to

* [geerlingguy.nodejs](https://github.com/geerlingguy/ansible-role-nodejs) - Node.js templates

## License
 
The MIT License (MIT)
